package com.isi.appEtudiant.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Etudiant implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEtudiant;

    @Column(length = 100)
    private String matricule;

    @Column(length = 100)
    private String nom;

    @Column(length = 100)
    private String prenom;

    @ManyToOne
    @JoinColumn(name = "idClasse")
    private Classe classe;
}
