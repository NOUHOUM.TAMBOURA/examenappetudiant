package com.isi.appEtudiant.service;

import com.isi.appEtudiant.entities.Classe;
import com.isi.appEtudiant.entities.Etudiant;
import com.isi.appEtudiant.repository.ClasseRepository;
import com.isi.appEtudiant.repository.EtudiantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class EtudiantService {

    @Autowired
    private EtudiantRepository etudiantRepository;

    @Autowired
    private ClasseRepository classeRepository;

    public Etudiant addEtudiant(Etudiant etudiant) {
        // Obtenez la date et l'heure actuelles
        LocalDateTime now = LocalDateTime.now();

        Classe classe = listeByIdClasse(etudiant.getClasse().getIdClasse());
        classe.setEffecif(classe.getEffecif() + 1);
        String matricule = "Et-"
                .concat(String.valueOf(now.getDayOfMonth()))
                .concat(String.valueOf(now.getMonthValue()))
                .concat(String.valueOf(now.getYear()))
                .concat(String.valueOf(now.getHour()))
                .concat(String.valueOf(now.getMinute()))
                .concat(String.valueOf(now.getSecond()))
                .concat("-@");
        etudiant.setMatricule(matricule);

        etudiant.setClasse(classe);
        return etudiantRepository.save(etudiant);
    }

    public List<Etudiant> listEtudiant() {
        List<Etudiant> lists = etudiantRepository.findAll(Sort.by("nom").ascending());
        return lists;
    }

    public List<Classe> listClasse() {
        List<Classe> lists = classeRepository.findAll(Sort.by("idClasse").ascending());
        return lists;
    }

    public Etudiant listeById(long id) {
        return etudiantRepository.findById(id).get();
    }

    public Classe listeByIdClasse(long idClasse) {
        return classeRepository.findById(idClasse).get();
    }

    public Etudiant updateEtudiant(Etudiant etudiant) {
        Etudiant etudiants = listeById(etudiant.getIdEtudiant());
        if (!etudiants.equals(null)) {
            Classe classe = listeByIdClasse(etudiant.getClasse().getIdClasse());
            if (etudiants.getClasse().getIdClasse() != classe.getIdClasse()) {

                /**
                 * pour changer la classe de etudiant et incrementer l'effectif
                 */
                classe.setEffecif(classe.getEffecif() + 1);
                classeRepository.save(classe);

                /**
                 * pour changer la classe de etudiant et decrementer l'effectif
                 */
                Classe classeDecrement = etudiants.getClasse();
                classeDecrement.setEffecif(classeDecrement.getEffecif() - 1);
                classeRepository.save(classeDecrement);
            }
            etudiant = etudiantRepository.save(etudiant);

        } else {
            throw new RuntimeException("Etudiant n'exist pas");
        }
        return etudiant;
    }

    public void deleteEtudiant(long idEtudiant) {
        Etudiant etudiant = listeById(idEtudiant);
        if (!etudiant.equals(null)) {
            etudiantRepository.deleteById(idEtudiant);
            Classe classe = listeByIdClasse(etudiant.getClasse().getIdClasse());
            classe.setEffecif(classe.getEffecif() - 1);
            classeRepository.save(classe);

        } else {
            throw new RuntimeException();
        }
    }

}
