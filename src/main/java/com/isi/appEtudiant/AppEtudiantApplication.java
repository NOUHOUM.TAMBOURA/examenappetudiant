package com.isi.appEtudiant;

import com.isi.appEtudiant.entities.Classe;
import com.isi.appEtudiant.entities.Etudiant;
import com.isi.appEtudiant.repository.ClasseRepository;
import com.isi.appEtudiant.repository.EtudiantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppEtudiantApplication implements CommandLineRunner {

	@Autowired
	private EtudiantRepository etudiantRepository;

	@Autowired
	private ClasseRepository classeRepository;

	public static void main(String[] args) {
		SpringApplication.run(AppEtudiantApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Classe classe1 = Classe.builder()
				.nom("M1DSIA")
				.effecif(1)
				.build();
		Classe classe2 = Classe.builder()
				.nom("M2DSIA")
				.effecif(1)
				.build();

		Classe classe3 = Classe.builder()
				.nom("M2GL")
				.effecif(1)
				.build();

		Etudiant etudiant1 = Etudiant.builder()
				.nom("TAMBOURA")
				.prenom("Nouhoum")
				.matricule("Et-552023991629-@")
				.classe(classe2)
				.build();

		Etudiant etudiant2 = Etudiant.builder()
				.nom("TAMBOURA")
				.prenom("Nouhoum")
				.matricule("Et-552023211629-@")
				.classe(classe1)
				.build();

		Etudiant etudiant3 = Etudiant.builder()
				.nom("TAMBOURA")
				.prenom("Nouhoum")
				.matricule("Et-552003211629-@")
				.classe(classe3)
				.build();

		classeRepository.save(classe1);
		classeRepository.save(classe2);
		classeRepository.save(classe3);

		etudiantRepository.save(etudiant1);
		etudiantRepository.save(etudiant2);
		etudiantRepository.save(etudiant3);
	}
}
