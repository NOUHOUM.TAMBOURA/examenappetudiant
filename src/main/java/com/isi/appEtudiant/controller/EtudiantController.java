package com.isi.appEtudiant.controller;

import com.isi.appEtudiant.entities.Classe;
import com.isi.appEtudiant.entities.Etudiant;
import com.isi.appEtudiant.service.EtudiantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/etudiant/")
public class EtudiantController {

    @Autowired
    private EtudiantService etudiantService;

    @GetMapping(value = "/listClasse")
    public ResponseEntity<List<Classe>> listeClasse() {
        List<Classe> classeList = null;
        try {
            classeList = etudiantService.listClasse();
        } catch (Exception e) {
            throw new RuntimeException();
        }

        return new ResponseEntity<>(classeList, HttpStatus.OK);
    }

    @GetMapping (value = "listByIdEtudiant/{idEtudiant}")
    public Etudiant getByIdEtudiant(@PathVariable(name = "idEtudiant") long idEtudiant) {
        try {
            Etudiant etudiant = etudiantService.listeById(idEtudiant);
            return etudiant;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }


    @GetMapping (value = "listByIdClasse/{idClasse}")
    public Classe getById(@PathVariable(name = "idClasse") long idClasse) {
        try {
            Classe classe = etudiantService.listeByIdClasse(idClasse);
            return classe;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }


    @PostMapping(value = "/addEtudiant")
    ResponseEntity<Object> addEtudiant(@RequestBody Etudiant etudiant) {
        try {
            etudiant = etudiantService.addEtudiant(etudiant);
        } catch (Exception e) {
            throw new RuntimeException();
        }

        return new ResponseEntity<>(etudiant, HttpStatus.CREATED);
    }

    @GetMapping(value = "/listEtudiant")
    public ResponseEntity<List<Etudiant>> listeEtudiant() {
        List<Etudiant> etudiants = null;
        try {
            etudiants = etudiantService.listEtudiant();
        } catch (Exception e) {
            throw new RuntimeException();
        }

        return new ResponseEntity<>(etudiants, HttpStatus.OK);
    }

    @PostMapping(value = "/updateEtudiant/{id}")
    public ResponseEntity<Object> updateAgent(@PathVariable(name = "id") long id, @RequestBody Etudiant etudiant) {
        try {
            etudiant.setIdEtudiant(id);
            etudiantService.updateEtudiant(etudiant);
        } catch (Exception e) {
            throw new RuntimeException();
        }
        return new ResponseEntity<>(etudiant, HttpStatus.OK);
    }

    @DeleteMapping(value = "deleteById/{idEtudiant}")
    public ResponseEntity<Etudiant> deleteById(@PathVariable(name = "idEtudiant") long idEtudiant) {
        try {
            etudiantService.deleteEtudiant(idEtudiant);
        } catch (Exception e) {
            throw new RuntimeException();
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
